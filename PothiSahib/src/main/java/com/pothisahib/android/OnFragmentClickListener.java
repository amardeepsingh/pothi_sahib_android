package com.pothisahib.android;

/**
 * Created by as on 2013-10-05.
 */
public interface OnFragmentClickListener {

    void fragmentClicked();
}
